using System;
using System.Collections.Generic;
using System.Net.Http;
using Autofac;
using FluentAssertions;
using MortgageAmountCompute.Core;
using MortgageAmountCompute.DeltaCredit;
using MortgageAmountCompute.Sberbank;
using Xunit;

namespace MortgageAmountCompute.Tests
{
    public class ComponentResolveTests
    {
        static MortgageInfo _testMortgageInfo = new MortgageInfo(3312503.45M, 14, 365, 9.25M, 430, new DateTime(2019, 03, 28));

        [Fact]
        public void AppInstaller_OnInit_ShouldBeCreated()
        {
            var builder = new AppInstaller();
        }
        public static IEnumerable<object[]> KeyedTypes()
        {
            yield return new object[] { BankName.DeltaCreditBank, typeof(IMortgageCalculatorFormula), typeof(DeltaCalculatorFormula) };
            yield return new object[] { BankName.SberBank, typeof(IMortgageCalculatorFormula), typeof(SberCalculatorFormula) };
            yield return new object[] { BankName.DeltaCreditBank, typeof(IMortgageCalculatorFactory), typeof(DeltaMortgageCalculatorFactory) };
            yield return new object[] { BankName.SberBank, typeof(IMortgageCalculatorFactory), typeof(SberMortgageCalculatorFactory) };
        }

        public static IEnumerable<object[]> FactoryTypes()
        {
            yield return new object[] { BankName.SberBank, typeof(SberMortgageCalculator) };
            yield return new object[] { BankName.DeltaCreditBank, typeof(DeltaMortgageCalculator) };
        }

        [Theory]
        [MemberData(nameof(KeyedTypes))]
        public void KeyedComponents_WithNoFactory_ShouldResolveSuccessful(BankName bankName, object typeUnderTest, object desiredType)
        {
            var installer = new AppInstaller();
            var container = installer.Container;
            var item = container.ResolveKeyed(bankName, (Type)typeUnderTest);
            item.Should().BeAssignableTo((Type)desiredType);
        }

        [Theory]
        [MemberData(nameof(FactoryTypes))]
        public void ComponentTypes_ShouldResolveSuccessful(BankName bankName, object typeUnderTest)
        {
            var installer = new AppInstaller();
            var container = installer.Container;
            var item = container.ResolveKeyed<IMortgageCalculatorFactory>(bankName);
            item.Should().BeAssignableTo<IMortgageCalculatorFactory>();
            var calculator = item.Create(_testMortgageInfo);
            calculator.Should().BeAssignableTo((Type)typeUnderTest);
        }

        [Fact]
        public void MorgageCalculatorBuilder_ShouldResolveSuccessful()
        {
            var installer = new AppInstaller();
            var container = installer.Container;
            var item = container.Resolve<IMortgageCalculatorBuilder>();
            item.Should().BeAssignableTo<IMortgageCalculatorBuilder>();
        }

        [Fact]
        public void CsvBuilder_ShouldResolveSuccessful()
        {
            var installer = new AppInstaller();
            var container = installer.Container;
            var item = container.Resolve<ICsvBuilder>();
            item.Should().BeAssignableTo<ICsvBuilder>();
        }

        [Theory]
        [MemberData(nameof(FactoryTypes))]
        public void MorgageCalculatorBuilder_OnBuildInvoke_ShouldReturnTargetCalculator(BankName bankName, Type targetType)
        {
            var installer = new AppInstaller();
            var container = installer.Container;
            var item = container.Resolve<IMortgageCalculatorBuilder>();
            item.Should().BeAssignableTo<IMortgageCalculatorBuilder>();
            var calculator = item.Build(bankName, _testMortgageInfo);
            calculator.Should().BeAssignableTo(targetType);
        }
    }
}
