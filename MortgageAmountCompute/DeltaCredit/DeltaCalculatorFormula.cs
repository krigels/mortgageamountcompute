﻿using MortgageAmountCompute.Core;

namespace MortgageAmountCompute.DeltaCredit
{
    internal class DeltaCalculatorFormula : MortgageCalculatorFormula
    {
        internal DeltaCalculatorFormula() : base() { }
        public override decimal Calculate(decimal debitAmount, decimal computedPercentRank, int paymentPeriodsCount)
        {
            debitAmount.PositiveValue(nameof(debitAmount));
            computedPercentRank.PositiveValue(nameof(computedPercentRank));
            paymentPeriodsCount.PositiveValue(nameof(paymentPeriodsCount));

            var annuityAmount = debitAmount * (computedPercentRank/(1-(Power(1+computedPercentRank,-(paymentPeriodsCount-1)))));
            return annuityAmount;
        }
    }
}
