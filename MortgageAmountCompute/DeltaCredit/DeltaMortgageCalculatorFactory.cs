﻿using System;
using System.Collections.Generic;
using System.Text;
using MortgageAmountCompute.Core;

namespace MortgageAmountCompute.DeltaCredit
{
  internal class DeltaMortgageCalculatorFactory : IMortgageCalculatorFactory
  {
      private readonly Func<MortgageInfo, DeltaMortgageCalculator> _factory;

      internal DeltaMortgageCalculatorFactory(Func<MortgageInfo, DeltaMortgageCalculator> factory)
      {
        _factory = factory;
      }

      public IMortgageCalculator Create(MortgageInfo mortgageInfo)
      {
        return _factory(mortgageInfo);
      }

    }
}
