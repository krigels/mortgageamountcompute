﻿using System;
using System.Collections.Generic;
using MortgageAmountCompute.Core;

namespace MortgageAmountCompute.DeltaCredit
{
    internal class DeltaMortgageCalculator : MortgageCalculator
    {
        internal DeltaMortgageCalculator(MortgageInfo mortgage, IMortgageCalculatorFormula formula) : base(mortgage,
          formula)
        {
            BankName = BankName.DeltaCreditBank;
        }

        public override List<AnnuityPaymentInfo> ComputeAnnuityPayments()
        {
            DateTime periodStartDate = Mortgage.StartDate;
            int restPeriodsCount = Mortgage.PaymentPeriodsCount;
            decimal restDebitAmount = Mortgage.MortgageAmount;
            var computedPercentRank = recalculateFromDaysPerYear(Mortgage.ComputedPercentRank, periodStartDate);

            var annuityPaymentList = new List<AnnuityPaymentInfo>(restPeriodsCount);
            var annuityPaymentAmmount = Formula.Calculate(restDebitAmount, computedPercentRank, restPeriodsCount);

            for (int i = 1; i <= Mortgage.PaymentPeriodsCount; i++)
            {
                computedPercentRank = recalculateFromDaysPerYear(Mortgage.ComputedPercentRank, periodStartDate);

                if (i == 2) annuityPaymentAmmount = Formula.Calculate(restDebitAmount, computedPercentRank, restPeriodsCount);

                AnnuityPaymentInfo annuityPaymentInfo =
                  CreateAnnuityPaymentInfo(annuityPaymentAmmount, periodStartDate, restDebitAmount);

                if (i == 1)
                    annuityPaymentInfo =
                      new AnnuityPaymentInfo(annuityPaymentInfo, annuityPaymentInfo.AnnuityPaymentPercentsAmount, 0);

                if (restDebitAmount <= annuityPaymentAmmount)
                    annuityPaymentInfo = new AnnuityPaymentInfo(annuityPaymentInfo, restDebitAmount, restDebitAmount);

                annuityPaymentList.Add(annuityPaymentInfo);

                restPeriodsCount -= 1;
                restDebitAmount -= annuityPaymentInfo.AnnuityPaymentDebitBody;
                periodStartDate = annuityPaymentInfo.PeriodEndDate.AddDays(1);
            }

            return annuityPaymentList;
        }

        private decimal recalculateFromDaysPerYear(decimal mortgageComputedPercentRank, DateTime startPeriodDate)
        {
            var daysInYear = new DateTime(startPeriodDate.Year, 12, 31).DayOfYear;

            //todo[sk] check this out to increase compute precision
            //if (daysInYear == 366) return (mortgageComputedPercentRank * 365) / 366;
            return mortgageComputedPercentRank;
        }
    }
}
