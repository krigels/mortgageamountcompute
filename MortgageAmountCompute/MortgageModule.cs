﻿using System.Reflection;
using Autofac;
using Autofac.Core;
using MortgageAmountCompute.Core;
using MortgageAmountCompute.DeltaCredit;
using MortgageAmountCompute.Sberbank;
using Module = Autofac.Module;

namespace MortgageAmountCompute
{
    public class MortgageModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CsvBuilder>().As<ICsvBuilder>()
              .FindConstructorsWith(a => a.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic)).SingleInstance();

            builder.RegisterType<SberCalculatorFormula>().Keyed<IMortgageCalculatorFormula>(BankName.SberBank)
              .FindConstructorsWith(a => a.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic))
              .InstancePerDependency();
            builder.RegisterType<SberMortgageCalculator>().AsSelf().InstancePerDependency()
              .FindConstructorsWith(a => a.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic))
              .WithParameter(new ResolvedParameter(
                 (p, c) => p.Name == "formula",
                 (p, c) => c.ResolveKeyed<IMortgageCalculatorFormula>(BankName.SberBank)));

            builder.RegisterType<DeltaCalculatorFormula>().Keyed<IMortgageCalculatorFormula>(BankName.DeltaCreditBank)
               .FindConstructorsWith(a => a.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic))
               .InstancePerDependency();

            builder.RegisterType<DeltaMortgageCalculator>().AsSelf().InstancePerDependency()
              .FindConstructorsWith(a => a.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic))
                .WithParameter(new ResolvedParameter(
                  (p, c) => p.Name == "formula",
                  (p, c) => c.ResolveKeyed<IMortgageCalculatorFormula>(BankName.DeltaCreditBank)));

            builder.RegisterType<SberMortgageCalculatorFactory>().Keyed<IMortgageCalculatorFactory>(BankName.SberBank)
                .FindConstructorsWith(a => a.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic))
                .SingleInstance();
 
            builder.RegisterType<DeltaMortgageCalculatorFactory>().Keyed<IMortgageCalculatorFactory>(BankName.DeltaCreditBank)
              .FindConstructorsWith(a => a.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic))
              .SingleInstance();

            builder.RegisterType<MortgageCalculatorBuilder>().As<IMortgageCalculatorBuilder>()
              .FindConstructorsWith(a => a.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic))
              .SingleInstance();
        }
    }
}
