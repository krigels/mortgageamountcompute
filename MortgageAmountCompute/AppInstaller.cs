﻿using Autofac;

namespace MortgageAmountCompute
{
    internal class AppInstaller
    {
        internal IContainer Container { get; }

        public AppInstaller()
        {
            var containerBuilder = new ContainerBuilder();

            var mortgageModule = new MortgageModule();
            containerBuilder.RegisterModule(mortgageModule);
            Container = containerBuilder.Build();
        }

        public T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }
}
