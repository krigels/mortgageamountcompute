﻿using MortgageAmountCompute.Core;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MortgageAmountCompute.Tests")]
namespace MortgageAmountCompute
{
    partial class Program
    {
        static void Main(string[] args)
        {
            var appInstaller = new AppInstaller();

            var calculatorBuilder = appInstaller.Resolve<IMortgageCalculatorBuilder>();
            var csvBuilder = appInstaller.Resolve<ICsvBuilder>();

            DeltaMortgageCompute(calculatorBuilder, csvBuilder);
            SberMortgageCompute(calculatorBuilder, csvBuilder);
            SberNewParamsCompute(calculatorBuilder, csvBuilder);
        }

        private static void DeltaMortgageCompute(IMortgageCalculatorBuilder calculatorBuilder, ICsvBuilder csvBuilder)
        {
            var mortgageInfo = new MortgageInfo(3312503.45M, 14, 365, 9.25M, 430, new DateTime(2019, 03, 28));
            var calculator = calculatorBuilder.Build(BankName.DeltaCreditBank, mortgageInfo);
            var result = calculator.ComputeAnnuityPayments();
            csvBuilder.ToCsv(mortgageInfo, result, "delta_mortgage.csv");
        }
        private static void SberMortgageCompute(IMortgageCalculatorBuilder calculatorBuilder, ICsvBuilder csvBuilder)
        {
            var mortgageInfo = new MortgageInfo(3500000, 14, 365, 11.9M, 400, new DateTime(2015, 09, 01));
            var calculator = calculatorBuilder.Build(BankName.SberBank, mortgageInfo);
            var result = calculator.ComputeAnnuityPayments();
            csvBuilder.ToCsv(mortgageInfo, result, "sber_mortgage.csv");
        }

        private static void SberNewParamsCompute(IMortgageCalculatorBuilder calculatorBuilder, ICsvBuilder csvBuilder)
        {
            var mortgageInfo = new MortgageInfo(3180003.31M, 11.9M, 198, new DateTime(2019, 03, 28));
            var calculator = calculatorBuilder.Build(BankName.SberBank, mortgageInfo);
            var result = calculator.ComputeAnnuityPayments();
            csvBuilder.ToCsv(mortgageInfo, result, "sber_mortgage_new.csv");
        }
    }
}
