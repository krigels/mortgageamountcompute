﻿using MortgageAmountCompute.Core;

namespace MortgageAmountCompute.Sberbank
{
    internal class SberMortgageCalculator : MortgageCalculator
    {
        internal SberMortgageCalculator(MortgageInfo mortgage, IMortgageCalculatorFormula formula) : base(mortgage, formula)
        {
            BankName = BankName.SberBank;
        }
    }
}
