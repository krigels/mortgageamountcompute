﻿using System;
using System.Collections.Generic;
using System.Text;
using MortgageAmountCompute.Core;

namespace MortgageAmountCompute.Sberbank
{
    internal class SberMortgageCalculatorFactory : IMortgageCalculatorFactory
    {
        private readonly Func<MortgageInfo, SberMortgageCalculator> _factory;

        internal SberMortgageCalculatorFactory(Func<MortgageInfo, SberMortgageCalculator> factory)
        {
            _factory = factory;
        }

        public IMortgageCalculator Create(MortgageInfo mortgageInfo)
        {
            return _factory(mortgageInfo);
        }
    }
}
