﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MortgageAmountCompute.Core
{
    internal class CsvBuilder : ICsvBuilder
    {
        internal CsvBuilder() { }
        public void ToCsv(MortgageInfo mortgageInfo, List<AnnuityPaymentInfo> result, string fileName)
        {
            mortgageInfo.NotNull(nameof(mortgageInfo));
            fileName.NotNullOrEmpty(nameof(fileName));

            if (result.IsNullOrEmpty())
            {
                return;
            }

            StringBuilder builder = exportToBuilder(mortgageInfo, result);
            System.IO.File.WriteAllText(fileName, builder.ToString(), Encoding.UTF8);
        }

        private StringBuilder exportToBuilder(MortgageInfo mortgageInfo,
          List<AnnuityPaymentInfo> result)
        {
            var builder = new StringBuilder();

            appendHeader(builder, mortgageInfo, result);
            builder.Append(Environment.NewLine);

            appendItemHeader(builder);
            builder.Append(Environment.NewLine);

            result.ForEach(a =>
            {
                appendItem(builder, a);
                builder.Append(Environment.NewLine);
            });

            return builder;
        }

        private void appendItemHeader(StringBuilder builder)
        {
            builder.Append("Сумма аннуитетного платежа");
            builder.Append(";");
            builder.Append("Дата начала периода");
            builder.Append(";");
            builder.Append("Дата окончания периода");
            builder.Append(";");
            builder.Append("Остаток задолженности на начало текущего периода");
            builder.Append(";");
            builder.Append("Сумма погашения процентов в аннуитетном платеже");
            builder.Append(";");
            builder.Append("Сумма погашения долга в аннуитетном платеже");
            builder.Append(";");
        }

        private static void appendItem(StringBuilder builder, AnnuityPaymentInfo annuityPaymentInfo)
        {
            builder.Append(round(annuityPaymentInfo.AnnuityPayment));
            builder.Append(";");
            builder.Append(annuityPaymentInfo.PeriodStartDate);
            builder.Append(";");
            builder.Append(annuityPaymentInfo.PeriodEndDate);
            builder.Append(";");
            builder.Append(round(annuityPaymentInfo.CurrentPeriodDebitRest));
            builder.Append(";");
            builder.Append(round(annuityPaymentInfo.AnnuityPaymentPercentsAmount));
            builder.Append(";");
            builder.Append(round(annuityPaymentInfo.AnnuityPaymentDebitBody));
            builder.Append(";");
        }

        private static void appendHeader(StringBuilder builder, MortgageInfo mortgageInfo,
          List<AnnuityPaymentInfo> payments)
        {
            var wholePaymentsAmount = payments.Sum(a => a.AnnuityPayment);
            var bankReward = payments.Sum(a => a.AnnuityPaymentPercentsAmount);

            builder.Append("Сумма кредита");
            builder.Append(";");
            builder.Append("Процентная ставка");
            builder.Append(";");
            builder.Append("Количество периодов оплаты");
            builder.Append(";");
            builder.Append("Дата начала");
            builder.Append(";");
            builder.Append("Общая стоимость кредита");
            builder.Append(";");
            builder.Append("Переплата банку");
            builder.Append(";");

            builder.Append(Environment.NewLine);

            builder.Append(mortgageInfo.MortgageAmount);
            builder.Append(";");
            builder.Append(mortgageInfo.PercentRank);
            builder.Append(";");
            builder.Append(mortgageInfo.PaymentPeriodsCount);
            builder.Append(";");
            builder.Append(mortgageInfo.StartDate);
            builder.Append(";");
            builder.Append(wholePaymentsAmount);
            builder.Append(";");
            builder.Append(bankReward);
            builder.Append(";");

            builder.Append(Environment.NewLine);
        }

        private static decimal round(decimal value, int prescision = 2)
        {
            return Math.Round(value, 2);
        }
    }
}
