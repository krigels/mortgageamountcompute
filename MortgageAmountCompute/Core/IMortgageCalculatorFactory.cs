﻿namespace MortgageAmountCompute.Core
{
    internal interface IMortgageCalculatorFactory
    {
        IMortgageCalculator Create(MortgageInfo mortgageInfo);
    }
}