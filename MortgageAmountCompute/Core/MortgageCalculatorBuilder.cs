﻿using System;
using Autofac.Features.Indexed;

namespace MortgageAmountCompute.Core
{
    internal class MortgageCalculatorBuilder : IMortgageCalculatorBuilder
    {
        private readonly IIndex<BankName, IMortgageCalculatorFactory> _serviceFactories;

        internal MortgageCalculatorBuilder(IIndex<BankName, IMortgageCalculatorFactory> serviceFactories)
        {
            _serviceFactories = serviceFactories;
        }

        public IMortgageCalculator Build(BankName bankName, MortgageInfo mortgageInfo)
        {
            mortgageInfo.NotNull(nameof(mortgageInfo));

            if (!_serviceFactories.TryGetValue(bankName, out var factory))
            {
                throw new InvalidOperationException($"Factory for {bankName} is not registered");
            }

            var calculator = _serviceFactories[bankName].Create(mortgageInfo);

            if (calculator == null)
            {
                throw new InvalidOperationException($"Unable to build calculator for {bankName} with {mortgageInfo}");
            }

            return calculator;
        }
    }
}
