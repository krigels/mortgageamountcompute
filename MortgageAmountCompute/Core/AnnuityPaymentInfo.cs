﻿using System;

namespace MortgageAmountCompute.Core
{
    internal class AnnuityPaymentInfo
    {
        public AnnuityPaymentInfo(AnnuityPaymentInfo annuityPaymentInfo, decimal annuityPayment,
          decimal annuityPaymentDebitBody)
        {
            annuityPaymentInfo.NotNull(nameof(annuityPaymentInfo));
            annuityPayment.PositiveValue(nameof(annuityPayment), true);
            annuityPaymentDebitBody.PositiveValue(nameof(annuityPaymentDebitBody), true);

            AnnuityPayment = annuityPayment;
            PeriodStartDate = annuityPaymentInfo.PeriodStartDate;
            PeriodEndDate = annuityPaymentInfo.PeriodEndDate;
            CurrentPeriodDebitRest = annuityPaymentInfo.CurrentPeriodDebitRest;
            ComputedPercentRank = annuityPaymentInfo.ComputedPercentRank;
            AnnuityPaymentPercentsAmount = annuityPayment - annuityPaymentDebitBody;
            AnnuityPaymentDebitBody = annuityPaymentDebitBody;
        }

        public AnnuityPaymentInfo(decimal annuityPayment, DateTime periodStartDate, int annuityPaymentPeriodDaysCount,
          decimal currentPeriodDebitRest, decimal computedPercentRank) :
          this(annuityPayment, periodStartDate, currentPeriodDebitRest, computedPercentRank)
        {
            annuityPaymentPeriodDaysCount.PositiveValue(nameof(annuityPaymentPeriodDaysCount));
            PeriodEndDate = PeriodStartDate.AddDays(annuityPaymentPeriodDaysCount - 1);
        }

        public AnnuityPaymentInfo(decimal annuityPayment, DateTime periodStartDate,
          decimal currentPeriodDebitRest, decimal computedPercentRank)
        {
            annuityPayment.PositiveValue(nameof(annuityPayment));
            periodStartDate.NotDefault(nameof(periodStartDate));
            currentPeriodDebitRest.PositiveValue(nameof(currentPeriodDebitRest), true);
            computedPercentRank.PositiveValue(nameof(computedPercentRank));

            AnnuityPayment = annuityPayment;
            PeriodStartDate = periodStartDate;
            PeriodEndDate = PeriodStartDate.AddMonths(1).AddDays(-1);
            CurrentPeriodDebitRest = currentPeriodDebitRest;
            ComputedPercentRank = computedPercentRank;
            AnnuityPaymentPercentsAmount = CurrentPeriodDebitRest * ComputedPercentRank;
            AnnuityPaymentDebitBody = AnnuityPayment - AnnuityPaymentPercentsAmount;
        }

        /// <summary>
        /// Сумма аннуитетного платежа 
        /// </summary>
        internal decimal AnnuityPayment { get; }

        /// <summary>
        /// Дата начала периода
        /// </summary>
        public DateTime PeriodStartDate { get; }

        /// <summary>
        /// Дата окончания периода
        /// </summary>
        public DateTime PeriodEndDate { get; }

        /// <summary>
        /// Остаток задолженности на начало текущего периода
        /// </summary>
        internal decimal CurrentPeriodDebitRest { get; }

        /// <summary>
        /// Сумма погашения процентов в аннуитетном платеже
        /// </summary>
        internal decimal AnnuityPaymentPercentsAmount { get; }

        /// <summary>
        /// Сумма погашения долга в аннуитетном платеже
        /// </summary>
        internal decimal AnnuityPaymentDebitBody { get; }

        /// <summary>
        /// Расчитанная процентная ставка с учетом периода
        /// </summary>
        internal decimal ComputedPercentRank { get; }
    }
}
