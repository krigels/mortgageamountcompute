﻿namespace MortgageAmountCompute.Core
{
    internal interface IMortgageCalculatorFormula
    {
        decimal Calculate(decimal debitAmount, decimal computedPercentRank, int paymentPeriodsCount);
    }
}