﻿using System.Collections.Generic;

namespace MortgageAmountCompute.Core
{
    internal interface ICsvBuilder
    {
        void ToCsv(MortgageInfo mortgageInfo, List<AnnuityPaymentInfo> result, string fileName);
    }
}