﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MortgageAmountCompute.Core
{
    public static class GuardExtensions
    {
        public static void PositiveValue(this int value, string message = null, bool allowZero = false)
        {
            if (!allowZero)
            {
                if (value <= 0)
                {
                    throw new ArgumentException(message ?? "Value must be greater than 0");
                }
            }
            else
            {
                if (value < 0)
                {
                    throw new ArgumentException(message ?? "Value must be positive");
                }
            }
        }

        public static void PositiveValue(this decimal value, string message = null, bool allowZero = false)
        {
            if (!allowZero)
            {
                if (value <= 0)
                {
                    throw new ArgumentException(message ?? "Value must be greater than 0");
                }
            }
            else
            {
                if (value < 0)
                {
                    throw new ArgumentException(message ?? "Value must be positive");
                }
            }
        }

        public static void NotDefault(this DateTime value, string message = null)
        {
            if (value == default)
            {
                throw new ArgumentException(message ?? "Value must be set");
            }
        }

        public static void NotNull(this object value, string message = null)
        {
            if (value == null)
            {
                throw new ArgumentException(message ?? "Value must be set");
            }
        }

        public static void NotNullOrEmpty(this string value, string message = null)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException(message ?? "Value must be set");
            }
        }

        public static void NotNullOrEmpty<T>(this IEnumerable<T> value, string message = null)
        {
            if (value == null || !value.Any())
            {
                throw new ArgumentException(message ?? "Value must be set");
            }
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> value, string message = null)
        {
            if (value == null || !value.Any())
            {
                return true;
            }

            return false;
        }
    }
}
