﻿using System;
using System.Collections.Generic;

namespace MortgageAmountCompute.Core
{
    internal class MortgageCalculator : IMortgageCalculator
    {
        protected readonly MortgageInfo Mortgage;
        protected readonly IMortgageCalculatorFormula Formula;
        public BankName BankName { get; protected set; }

        internal MortgageCalculator(MortgageInfo mortgage, IMortgageCalculatorFormula formula)
        {
            mortgage.NotNull(nameof(mortgage));
            formula.NotNull(nameof(formula));

            Mortgage = mortgage;
            Formula = formula;
        }

        public virtual List<AnnuityPaymentInfo> ComputeAnnuityPayments()
        {
            DateTime periodStartDate = Mortgage.StartDate;
            int restPeriodsCount = Mortgage.PaymentPeriodsCount;
            decimal restDebitAmount = Mortgage.MortgageAmount;

            var annuityPaymentList = new List<AnnuityPaymentInfo>(restPeriodsCount);

            for (int i = 1; i <= Mortgage.PaymentPeriodsCount; i++)
            {
                var annuityPaymentAmmount =
                  Formula.Calculate(restDebitAmount, Mortgage.ComputedPercentRank, restPeriodsCount);
                AnnuityPaymentInfo annuityPaymentInfo =
                  CreateAnnuityPaymentInfo(annuityPaymentAmmount, periodStartDate, restDebitAmount);
                annuityPaymentList.Add(annuityPaymentInfo);

                restPeriodsCount -= 1;
                restDebitAmount -= annuityPaymentInfo.AnnuityPaymentDebitBody;
                periodStartDate = annuityPaymentInfo.PeriodEndDate.AddDays(1);
            }

            return annuityPaymentList;
        }

        protected AnnuityPaymentInfo CreateAnnuityPaymentInfo(decimal annuityPaymentAmmount, DateTime periodStartDate,
          decimal restDebitAmount)
        {
            if (Mortgage.IsMonthlyComputed)
                return new AnnuityPaymentInfo(annuityPaymentAmmount, periodStartDate, restDebitAmount,
                  Mortgage.ComputedPercentRank);
            return new AnnuityPaymentInfo(annuityPaymentAmmount, periodStartDate,
              Mortgage.AnnuityPaymentPeriodDaysCount, restDebitAmount, Mortgage.ComputedPercentRank);
        }
    }
}
