﻿namespace MortgageAmountCompute.Core
{
    internal interface IMortgageCalculatorBuilder
    {
        IMortgageCalculator Build(BankName bankName, MortgageInfo mortgageInfo);
    }
}