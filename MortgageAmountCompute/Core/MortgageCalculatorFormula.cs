﻿using System;

namespace MortgageAmountCompute.Core
{
    internal class MortgageCalculatorFormula : IMortgageCalculatorFormula
    {
        public virtual decimal Calculate(decimal debitAmount, decimal computedPercentRank, int paymentPeriodsCount)
        {
            debitAmount.PositiveValue(nameof(debitAmount));
            computedPercentRank.PositiveValue(nameof(computedPercentRank));
            paymentPeriodsCount.PositiveValue(nameof(paymentPeriodsCount));

            var annuityAmount = (debitAmount * computedPercentRank) /
                                (1 - Power((1 + computedPercentRank), -paymentPeriodsCount));

            return annuityAmount;
        }

        protected decimal Power(decimal amount, int power)
        {
            decimal result = 1;
            if (power == 0) return result;

            for (int i = 0; i < Math.Abs(power); i++)
            {
                result = result * amount;
            }

            if (power < 0)
                return 1 / result;

            return result;
        }
    }
}
