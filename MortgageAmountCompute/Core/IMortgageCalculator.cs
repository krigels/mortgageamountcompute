﻿using System.Collections.Generic;

namespace MortgageAmountCompute.Core
{
    internal interface IMortgageCalculator
    {
        List<AnnuityPaymentInfo> ComputeAnnuityPayments();
    }
}