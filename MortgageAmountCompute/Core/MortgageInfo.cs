﻿using System;

namespace MortgageAmountCompute.Core
{
    internal class MortgageInfo
    {
        /// <summary>
        /// Общая сумма кредита
        /// </summary>
        internal decimal MortgageAmount { get; }

        /// <summary>
        /// Количество дней в расчетном периоде
        /// </summary>
        internal int AnnuityPaymentPeriodDaysCount { get; }

        /// <summary>
        /// Количество дней в году
        /// </summary>
        internal int YearDaysCount { get; }

        /// <summary>
        /// Процентная ставка
        /// </summary>
        internal decimal PercentRank { get; }

        /// <summary>
        /// Количество расчетных периодов
        /// </summary>
        internal int PaymentPeriodsCount { get; }

        /// <summary>
        /// Расчитанная процентная ставка с учетом периода
        /// </summary>
        internal decimal ComputedPercentRank { get; }

        /// <summary>
        /// Флаг показывающий целые месячные периоды
        /// </summary>
        internal bool IsMonthlyComputed { get; }

        /// <summary>
        /// Дата начала 
        /// </summary>
        internal DateTime StartDate { get; }

        public MortgageInfo(decimal mortgageAmount, int annuityPaymentPeriodDaysCount, int yearDaysCount,
          decimal percentRank, int paymentPeriodsCount, DateTime startDate)
        {
            mortgageAmount.PositiveValue(nameof(mortgageAmount));
            annuityPaymentPeriodDaysCount.PositiveValue(nameof(annuityPaymentPeriodDaysCount));
            yearDaysCount.PositiveValue(nameof(yearDaysCount));
            percentRank.PositiveValue(nameof(percentRank));
            paymentPeriodsCount.PositiveValue(nameof(paymentPeriodsCount));
            startDate.NotDefault(nameof(startDate));

            MortgageAmount = mortgageAmount;
            AnnuityPaymentPeriodDaysCount = annuityPaymentPeriodDaysCount;
            YearDaysCount = yearDaysCount;
            PercentRank = percentRank;
            PaymentPeriodsCount = paymentPeriodsCount;

            ComputedPercentRank = (PercentRank / 100) * ((decimal)AnnuityPaymentPeriodDaysCount / YearDaysCount);
            IsMonthlyComputed = false;
            StartDate = startDate;
        }

        public MortgageInfo(decimal mortgageAmount, decimal percentRank, int paymentPeriodsCount, DateTime startDate)
          : this(mortgageAmount, 1, 12, percentRank, paymentPeriodsCount, startDate)
        {
            IsMonthlyComputed = true;
        }


        public override string ToString()
        {
            return
              $"MortgageAmount:{MortgageAmount} AnnuityPaymentPeriodDaysCount:{AnnuityPaymentPeriodDaysCount} YearDaysCount:{YearDaysCount} PercentRank:{PercentRank}" +
              $"PaymentPeriodsCount: {PaymentPeriodsCount}  ComputedPercentRank:{ComputedPercentRank} IsMonthlyComputed:{IsMonthlyComputed}";
        }
    }
}
